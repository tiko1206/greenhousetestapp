package com.ryzee.greenhouseapp.manager.navigator

import com.ryzee.greenhouseapp.presentation.Destination
import kotlinx.coroutines.flow.StateFlow

interface NavigatorManager {
    val navigationEvent: StateFlow<NavigationEvent?>
    fun navigate(destination: Destination, popCurrent: Boolean = false)
    fun release()
}
package com.ryzee.greenhouseapp.manager.wifiConnection

import com.ryzee.greenhouseapp.data.model.Result
import com.ryzee.greenhouseapp.di.ApplicationScope
import com.ryzee.greenhouseapp.manager.wifiConnection.QRWiFiInfoParser.QRWiFiInfo
import com.ryzee.greenhouseapp.manager.wifiConnection.WiFiConnector.WiFiConnectionStatus
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.onEach
import kotlinx.coroutines.launch
import javax.inject.Inject
import javax.inject.Singleton


@Singleton
class WifiConnectionManagerImpl @Inject constructor(
    @ApplicationScope private val applicationScope: CoroutineScope,
    private val wifiInfoParser: QRWiFiInfoParser,
    private val wiFiConnector: WiFiConnector
) : WifiConnectionManager {

    private val _connectionResult = MutableStateFlow<Result<WiFiConnectionStatus>>(Result.Error())
    override val connectionResult: StateFlow<Result<WiFiConnectionStatus>> = _connectionResult

    private var currentWifiInfo: QRWiFiInfo? = null

    override fun verifyQRCode(scanResult: String): Boolean {
        return try {
            wifiInfoParser.parse(scanResult)
            true
        } catch (e: Exception) {
            false
        }
    }

    override fun connect(scanResult: String) {
        val wifiInfo = wifiInfoParser.parse(scanResult)
        val currentResult = _connectionResult.value
        if (currentWifiInfo != wifiInfo || currentResult !is Result.Success) {
            applicationScope.launch {
                wiFiConnector.connectToWiFi(wifiInfo)
                    .onEach {
                        if(it is WiFiConnectionStatus.Connected) {
                            currentWifiInfo = wifiInfo
                        }
                        _connectionResult.value = it.toResult()
                    }
                    .collect()
            }
        } else {
            _connectionResult.value = currentResult.data.toResult()
        }
    }

    private fun WiFiConnectionStatus.toResult() = when (this) {
        is WiFiConnectionStatus.Connected -> Result.Success(this)
        is WiFiConnectionStatus.CouldNotConnect -> Result.Error(WiFiConnector.WiFiConnectionException())
        is WiFiConnectionStatus.Disconnected -> Result.Error(WiFiConnector.WiFiDisconnectedException())
    }

}
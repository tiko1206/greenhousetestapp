package com.ryzee.greenhouseapp.manager.wifiConnection

import com.ryzee.greenhouseapp.data.model.Result
import kotlinx.coroutines.flow.StateFlow

interface WifiConnectionManager {
    val connectionResult: StateFlow<Result<WiFiConnector.WiFiConnectionStatus>>
    fun verifyQRCode(scanResult: String): Boolean
    fun connect(scanResult: String)
}
package com.ryzee.greenhouseapp.manager.wifiConnection

import android.util.Base64
import java.nio.charset.Charset
import javax.crypto.Cipher
import javax.crypto.spec.IvParameterSpec
import javax.crypto.spec.SecretKeySpec
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class QRWiFiInfoParser @Inject constructor() {

    private val keyBase64 = "ZFNnVmtZcDNzNnY5eS9CP0UoSCtNYlFlVGhXbVpxNHQ="
    private val delimiter = ";;"

    fun parse(qrPayload: String): QRWiFiInfo {
        val decrypted = decrypt(qrPayload)
        return if(decrypted.contains("IP:")){
            parseMock(decrypted)
        } else {
            parseReal(decrypted)
        }
    }

    private fun parseReal(decrypted: String): QRWiFiInfo {
        val split = decrypted.split(delimiter)
        return QRWiFiInfo(
            ssid = split.find { it.startsWith("S:") }!!.replace("S:", ""),
            passphrase = split.find { it.startsWith("P:") }!!.replace("P:", ""),
            isHiddenSsid = split.find { it.startsWith("H:") }!!.replace("H:", "").toBoolean()
        )
    }

    private fun parseMock(decrypted: String): QRWiFiInfo {
        return QRWiFiInfo(
            mockIp = decrypted.replace("IP:", "")
        )
    }

    private fun decrypt(qrPayload: String): String {
        val decoded = Base64.decode(qrPayload, Base64.NO_PADDING)
        val iv = decoded.take(16).toByteArray()
        val encryptedMessage = decoded.drop(16).toByteArray()

        val keyByteArray = Base64.decode(keyBase64, Base64.NO_PADDING)
        val aesSecretKeySpec = SecretKeySpec(keyByteArray, "AES")
        val ivSpec = IvParameterSpec(iv)
        val aesCipher = Cipher.getInstance("AES/CBC/PKCS5Padding")

        aesCipher.init(Cipher.DECRYPT_MODE, aesSecretKeySpec, ivSpec)

        val decryptedMessageBytes = aesCipher.doFinal(encryptedMessage)
        return String(decryptedMessageBytes, Charset.forName("UTF-8"))
    }

    data class QRWiFiInfo(
        val ssid: String = "",
        val passphrase: String = "",
        val isHiddenSsid: Boolean = false,
        val mockIp: String? = null
    )

}
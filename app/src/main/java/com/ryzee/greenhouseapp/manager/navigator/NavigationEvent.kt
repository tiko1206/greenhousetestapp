package com.ryzee.greenhouseapp.manager.navigator

import com.ryzee.greenhouseapp.presentation.Destination

class NavigationEvent(
    val destination: Destination,
    val popCurrent: Boolean
)
package com.ryzee.greenhouseapp.manager.wifiConnection

import android.content.Context
import android.net.*
import android.net.wifi.WifiManager
import android.net.wifi.WifiNetworkSpecifier
import android.os.Build
import android.text.format.Formatter
import com.ryzee.greenhouseapp.manager.wifiConnection.WiFiConnector.WiFiConnectionStatus.*
import dagger.hilt.android.qualifiers.ApplicationContext
import kotlinx.coroutines.CancellationException
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.cancel
import kotlinx.coroutines.channels.ProducerScope
import kotlinx.coroutines.channels.awaitClose
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.callbackFlow
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class WiFiConnector @Inject constructor(@ApplicationContext private val context: Context) {

    @OptIn(ExperimentalCoroutinesApi::class)
    fun connectToWiFi(wifiInfo: QRWiFiInfoParser.QRWiFiInfo) = callbackFlow {
        if(wifiInfo.mockIp != null) {
            connectToWiFiMock(wifiInfo, this)
        } else {
            connectToWiFiReal(wifiInfo, this)
        }
    }

    @ExperimentalCoroutinesApi
    private suspend fun connectToWiFiMock(wifiInfo: QRWiFiInfoParser.QRWiFiInfo, producer: ProducerScope<WiFiConnectionStatus>) = producer.apply {
        delay(2000)
        trySend(Connected(wifiInfo.mockIp!!))

        awaitClose {
            channel.close()
        }
    }

    @ExperimentalCoroutinesApi
    private suspend fun connectToWiFiReal(wifiInfo: QRWiFiInfoParser.QRWiFiInfo, producer: ProducerScope<WiFiConnectionStatus>) = producer.apply {
        val wifiManager = context.getSystemService(Context.WIFI_SERVICE) as WifiManager
        val connectivityManager = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager

        if(!wifiManager.isWifiEnabled) {
            cancel(WiFiDisabledException())
        }

        val wifiNetworkSpecifier = WifiNetworkSpecifier.Builder()
            .setSsid(wifiInfo.ssid)
            .setWpa2Passphrase(wifiInfo.passphrase)
            .setIsHiddenSsid(wifiInfo.isHiddenSsid)
            .build()

        val networkRequest = NetworkRequest.Builder()
            .addTransportType(NetworkCapabilities.TRANSPORT_WIFI)
            .setNetworkSpecifier(wifiNetworkSpecifier)
            .build()

        val callback = object : ConnectivityManager.NetworkCallback(){

            override fun onAvailable(network: Network) {
                connectivityManager.bindProcessToNetwork(network)
            }

            override fun onLinkPropertiesChanged(network: Network, linkProperties: LinkProperties) {
                trySend(Connected(getServerAddress(wifiManager, linkProperties)))
            }

            override fun onUnavailable() {
                trySend(CouldNotConnect)
                cancel(WiFiConnectionException())
            }

            override fun onLost(network: Network) {
                trySend(Disconnected)
                cancel(WiFiDisconnectedException())
            }
        }

        connectivityManager.requestNetwork(networkRequest, callback)

        awaitClose {
            connectivityManager.unregisterNetworkCallback(callback)
            channel.close()
        }
    }

    private fun getServerAddress(wifiManager: WifiManager, linkProperties: LinkProperties): String {
        fun getServerAddressLegacy(): String {
            val dhcp = wifiManager.dhcpInfo
            return Formatter.formatIpAddress(dhcp.gateway)
        }

        return if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.R) {
            linkProperties.dhcpServerAddress?.hostAddress ?: getServerAddressLegacy()
        } else {
            getServerAddressLegacy()
        }
    }


    sealed class WiFiConnectionStatus {
        data class Connected(val serverAddress: String): WiFiConnectionStatus()
        object Disconnected: WiFiConnectionStatus()
        object CouldNotConnect: WiFiConnectionStatus()
    }

    class WiFiDisabledException : CancellationException("WiFi is not enabled, enable it and retry")
    class WiFiConnectionException : CancellationException("It was not possible to connect to this WiFi")
    class WiFiDisconnectedException : CancellationException("The WiFi you were connected to was disconnected")
}
package com.ryzee.greenhouseapp.manager.navigator

import com.ryzee.greenhouseapp.presentation.Destination
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class NavigatorManagerImpl @Inject constructor() : NavigatorManager {
    private val _navigationEvent = MutableStateFlow<NavigationEvent?>(null)
    override var navigationEvent: StateFlow<NavigationEvent?> = _navigationEvent

    override fun navigate(destination: Destination, popCurrent: Boolean) {
        _navigationEvent.value = NavigationEvent(destination, popCurrent)
    }

    override fun release() {
        _navigationEvent.value = null
    }
}
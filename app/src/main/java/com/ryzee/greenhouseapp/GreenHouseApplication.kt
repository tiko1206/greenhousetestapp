package com.ryzee.greenhouseapp

import android.app.Application
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
class GreenHouseApplication: Application()
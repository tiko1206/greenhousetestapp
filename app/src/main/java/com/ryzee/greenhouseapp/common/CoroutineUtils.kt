package com.ryzee.greenhouseapp.common

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.*
import javax.inject.Qualifier

fun ViewModel.launchOnMain(block: suspend CoroutineScope.() -> Unit) =
    viewModelScope.launch(block = block)

fun ViewModel.launchOnBackground(block: suspend CoroutineScope.() -> Unit) =
    viewModelScope.launch { withContext(Dispatchers.Default, block) }
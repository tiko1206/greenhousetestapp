package com.ryzee.greenhouseapp.common

fun <T> MutableList<T>.clearAddAll(items: List<T>) {
    clear()
    addAll(items)
}
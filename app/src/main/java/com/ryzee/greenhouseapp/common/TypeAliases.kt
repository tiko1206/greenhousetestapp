package com.ryzee.greenhouseapp.common

typealias ComposeState<T> = androidx.compose.runtime.State<T>
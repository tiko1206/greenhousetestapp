package com.ryzee.greenhouseapp.di

import com.ryzee.greenhouseapp.data.repository.user.UserRepo
import com.ryzee.greenhouseapp.data.repository.user.UserRepoImpl
import com.ryzee.greenhouseapp.manager.navigator.NavigatorManager
import com.ryzee.greenhouseapp.manager.navigator.NavigatorManagerImpl
import com.ryzee.greenhouseapp.manager.wifiConnection.WifiConnectionManager
import com.ryzee.greenhouseapp.manager.wifiConnection.WifiConnectionManagerImpl
import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent

@Module
@InstallIn(SingletonComponent::class)
interface AppBindModule {
    @Binds
    fun bindUserRepo(userRepoImpl: UserRepoImpl): UserRepo

    @Binds
    fun bindNavigatorManager(navigatorManager: NavigatorManagerImpl): NavigatorManager

    @Binds
    fun bindWifiConnectionManager(wifiConnectionRepo: WifiConnectionManagerImpl): WifiConnectionManager
}
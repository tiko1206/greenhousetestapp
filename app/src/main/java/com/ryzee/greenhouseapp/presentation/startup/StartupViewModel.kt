package com.ryzee.greenhouseapp.presentation.startup

import androidx.lifecycle.ViewModel
import com.ryzee.greenhouseapp.manager.navigator.NavigatorManager
import com.ryzee.greenhouseapp.presentation.Destination
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

@HiltViewModel
class StartupViewModel @Inject constructor(navigatorManager: NavigatorManager): ViewModel() {
    init {
        navigatorManager.navigate(
            destination = Destination.WELCOME,
            popCurrent = true
        )
    }
}
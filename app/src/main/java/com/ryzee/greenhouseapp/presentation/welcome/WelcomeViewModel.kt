package com.ryzee.greenhouseapp.presentation.welcome

import androidx.lifecycle.ViewModel
import com.ryzee.greenhouseapp.manager.navigator.NavigatorManager
import com.ryzee.greenhouseapp.presentation.Destination
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

@HiltViewModel
class WelcomeViewModel @Inject constructor(
    private val navigatorManager: NavigatorManager
) : ViewModel() {

    fun checkCameraPermission(granted: Boolean) {
        if (granted)
            navigatorManager.navigate(Destination.QR_SCAN)
    }

}
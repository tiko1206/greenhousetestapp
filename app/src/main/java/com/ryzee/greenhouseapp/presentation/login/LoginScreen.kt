package com.ryzee.greenhouseapp.presentation.login

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.*
import androidx.compose.material.*
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.hilt.navigation.compose.hiltViewModel
import com.ryzee.greenhouseapp.R
import com.ryzee.greenhouseapp.presentation.theme.myColors

@Composable
fun LoginScreen(viewModel: LoginViewModel = hiltViewModel()) {
    Box(
        modifier = Modifier
            .fillMaxSize()
            .background(Color.White)
    ) {
        Column(
            modifier = Modifier
                .align(Alignment.Center)
                .background(
                    color = MaterialTheme.myColors.lightGray,
                    MaterialTheme.shapes.medium
                )
                .padding(start = 20.dp, end = 20.dp),
            horizontalAlignment = Alignment.CenterHorizontally
        ) {
            Text(
                text = stringResource(id = R.string.login),
                style = MaterialTheme.typography.h6,
                modifier = Modifier.padding(top = 25.dp)
            )
            TextField(
                modifier = Modifier.padding(top = 25.dp),
                value = viewModel.username,
                onValueChange = viewModel::onUserNameChanged,
                label = { Text(text = stringResource(id = R.string.username)) }
            )
            TextField(
                modifier = Modifier.padding(top = 20.dp),
                value = viewModel.password,
                onValueChange = viewModel::onPasswordChanged,
                label = { Text(text = stringResource(id = R.string.password)) }
            )
            Button(
                modifier = Modifier
                    .padding(top = 75.dp, bottom = 50.dp)
                    .height(50.dp)
                    .width(200.dp),
                colors = ButtonDefaults.buttonColors(
                    backgroundColor = Color.Black,
                    contentColor = Color.White
                ),
                onClick = {

                },
            ) {
                Text(text = stringResource(id = R.string.login))
            }
        }
    }
}

@Preview
@Composable
fun LoginScreenPreview() {
    LoginScreen()
}
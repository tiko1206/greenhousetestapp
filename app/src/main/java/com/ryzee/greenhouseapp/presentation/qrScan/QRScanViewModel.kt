package com.ryzee.greenhouseapp.presentation.qrScan

import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.setValue
import androidx.lifecycle.ViewModel
import com.ryzee.greenhouseapp.common.launchOnBackground
import com.ryzee.greenhouseapp.data.dataSource.api.provider.PublicApiProvider
import com.ryzee.greenhouseapp.data.model.Result
import com.ryzee.greenhouseapp.manager.navigator.NavigatorManager
import com.ryzee.greenhouseapp.manager.wifiConnection.WiFiConnector.WiFiConnectionStatus
import com.ryzee.greenhouseapp.manager.wifiConnection.WifiConnectionManager
import com.ryzee.greenhouseapp.presentation.Destination
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.drop
import javax.inject.Inject

@HiltViewModel
class QRScanViewModel @Inject constructor(
    private val wifiConnectionManager: WifiConnectionManager,
    private val navigatorManager: NavigatorManager,
    private val apiProvider: PublicApiProvider,
) : ViewModel() {

    var isScanning by mutableStateOf(true)
        private set

    val qrValidator: (String) -> Boolean = { input ->
        wifiConnectionManager.verifyQRCode(input)
    }

    init {
        launchOnBackground {
            wifiConnectionManager.connectionResult.drop(1).collect {
                when (it) {
                    is Result.Error -> startScanning()
                    is Result.Success -> {
                        if (it.data is WiFiConnectionStatus.Connected) {
                            //apiProvider.buildApi(it.data.serverAddress)
                            navigatorManager.navigate(Destination.LOGIN)
                        }
                    }
                }
            }
        }
    }

    fun onScanResult(result: String) = launchOnBackground {
        isScanning = false
        wifiConnectionManager.connect(result)
    }

    fun startScanning() {
        isScanning = true
    }

}
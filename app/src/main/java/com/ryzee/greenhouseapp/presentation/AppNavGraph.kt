package com.ryzee.greenhouseapp.presentation

import androidx.compose.animation.*
import androidx.compose.animation.core.tween
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.navigation.NavHostController
import com.ryzee.greenhouseapp.manager.navigator.NavigatorManager
import com.ryzee.greenhouseapp.presentation.login.LoginScreen
import com.ryzee.greenhouseapp.presentation.qrScan.QRScanScreen
import com.ryzee.greenhouseapp.presentation.startup.StartupScreen
import com.ryzee.greenhouseapp.presentation.welcome.WelcomeScreen
import com.google.accompanist.navigation.animation.AnimatedNavHost
import com.google.accompanist.navigation.animation.composable
import com.google.accompanist.navigation.animation.rememberAnimatedNavController

enum class Destination(val route: String) {
    STARTUP("startup"),
    QR_SCAN("qrscan"),
    WELCOME("welcome"),
    LOGIN("login")
}

@ExperimentalAnimationApi
@Composable
fun AppNavGraph(
    navigatorManager: NavigatorManager,
    navController: NavHostController = rememberAnimatedNavController()
) {
    val navigationEvent by navigatorManager.navigationEvent.collectAsState()
    LaunchedEffect(navigationEvent) {
        val event = navigatorManager.navigationEvent.value ?: return@LaunchedEffect
        if(event.popCurrent) {
            navController.popBackStack()
        }
        if(navController.currentDestination?.route != event.destination.route) {
            navController.navigate(event.destination.route)
        }
    }

    AnimatedNavHost(
        navController = navController,
        startDestination = Destination.STARTUP.route,
        enterTransition = {
            slideInHorizontally(
                initialOffsetX = { 1000 },
                animationSpec = tween(200)
            ) + fadeIn(animationSpec = tween(100))
        },
        exitTransition = {
            slideOutHorizontally(
                targetOffsetX = { -1000 },
                animationSpec = tween(200)
            ) + fadeOut(animationSpec = tween(100))
        },
        popEnterTransition = {
            slideInHorizontally(
                initialOffsetX = { -1000 },
                animationSpec = tween(200)
            ) + fadeIn(animationSpec = tween(100))
        },
        popExitTransition = {
            slideOutHorizontally(
                targetOffsetX = { 1000 },
                animationSpec = tween(200)
            ) + fadeOut(animationSpec = tween(100))
        }) {
        composable(Destination.STARTUP.route) { StartupScreen() }
        composable(Destination.WELCOME.route) { WelcomeScreen() }
        composable(Destination.QR_SCAN.route) { QRScanScreen() }
        composable(Destination.LOGIN.route) { LoginScreen() }
    }
}
package com.ryzee.greenhouseapp.presentation.qrScan.camera

import androidx.camera.core.CameraSelector
import androidx.camera.core.ImageAnalysis
import androidx.camera.core.Preview
import androidx.camera.lifecycle.ProcessCameraProvider
import androidx.camera.view.PreviewView
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.alpha
import androidx.compose.ui.geometry.Offset
import androidx.compose.ui.geometry.Size
import androidx.compose.ui.graphics.BlendMode
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.platform.LocalLifecycleOwner
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.unit.dp
import androidx.compose.ui.viewinterop.AndroidView
import androidx.core.content.ContextCompat
import androidx.lifecycle.LifecycleOwner
import com.google.common.util.concurrent.ListenableFuture
import com.ryzee.greenhouseapp.R

@Composable
fun CameraView(
    isScanning: Boolean,
    qrValidator: (String) -> Boolean,
    onClick: () -> Unit,
    onQrScanned: (String) -> Unit
) {

    val context = LocalContext.current
    val lifeCycleOwner = LocalLifecycleOwner.current

    val previewView = remember {
        PreviewView(context)
    }

    val preview = remember { Preview.Builder().build() }
    val selector = remember {
        CameraSelector.Builder()
            .requireLensFacing(CameraSelector.LENS_FACING_BACK)
            .build()
    }
    preview.setSurfaceProvider(previewView.surfaceProvider)

    val imageAnalysis = remember {
        ImageAnalysis.Builder()
            .setBackpressureStrategy(
                ImageAnalysis.STRATEGY_KEEP_ONLY_LATEST
            ).build()
    }

    val cameraProviderFuture = remember {
        ProcessCameraProvider.getInstance(context)
    }

    Box(modifier = Modifier
        .fillMaxSize()
        .clickable { onClick() }) {

        AndroidView(factory = { context ->
            imageAnalysis.setAnalyzer(
                ContextCompat.getMainExecutor(context),
                QrCodeAnalyzer(qrValidator) { result ->
                    onQrScanned(result)
                    unbind(cameraProviderFuture)
                }
            )
            previewView
        }, modifier = Modifier.fillMaxSize())
    }

    if (isScanning) {
        bindToLifecycle(
            cameraProviderFuture,
            lifeCycleOwner,
            selector,
            preview,
            imageAnalysis
        )
    }

    CameraFrame()

    Column(
        modifier = Modifier
            .fillMaxWidth()
            .padding(top = 100.dp),
        verticalArrangement = Arrangement.Center,
        horizontalAlignment = Alignment.CenterHorizontally
    ) {
        Text(text = stringResource(id = R.string.scan_qr_code), color = Color.White)
    }
}


private fun unbind(cameraProviderFuture: ListenableFuture<ProcessCameraProvider>) {
    cameraProviderFuture.get().unbindAll()
}


private fun bindToLifecycle(
    cameraProviderFuture: ListenableFuture<ProcessCameraProvider>,
    lifeCycleOwner: LifecycleOwner,
    selector: CameraSelector,
    preview: Preview,
    imageAnalysis: ImageAnalysis
) {
    try {
        cameraProviderFuture.get().bindToLifecycle(
            lifeCycleOwner,
            selector,
            preview,
            imageAnalysis
        )
    } catch (e: Exception) {
        e.printStackTrace()
    }
}

@Composable
private fun CameraFrame() {
    androidx.compose.foundation.Canvas(
        modifier = Modifier
            .fillMaxWidth()
            .alpha(0.6f)
            .fillMaxHeight()
    ) {

        drawRect(
            color = Color.LightGray,
            size = size
        )
        drawRect(
            color = Color.White,
            topLeft = Offset((size.width - 900) / 2f, (size.height - 900) / 2f),
            size = Size(900f, 900f),
            blendMode = BlendMode.DstOut
        )
    }
}
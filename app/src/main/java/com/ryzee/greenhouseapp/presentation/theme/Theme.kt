package com.ryzee.greenhouseapp.presentation.theme

import androidx.compose.foundation.isSystemInDarkTheme
import androidx.compose.material.MaterialTheme
import androidx.compose.material.darkColors
import androidx.compose.material.lightColors
import androidx.compose.runtime.Composable
import androidx.compose.runtime.CompositionLocalProvider
import androidx.compose.runtime.ReadOnlyComposable
import androidx.compose.runtime.staticCompositionLocalOf
import androidx.compose.ui.graphics.Color

//Example
private val warningColor = Color(0xFFFF1100)
private val warningColorDark = Color(0xFFFF8F00)

private val LightColorPalette = MyColors(
    material = lightColors(),
    warningColor = warningColor,
)

private val DarkColorPalette = MyColors(
    material = darkColors(),
    warningColor = warningColorDark,
)

private val LocalColors = staticCompositionLocalOf { LightColorPalette }

@Composable
fun GreenHouseTestAppTheme(
    darkTheme: Boolean = isSystemInDarkTheme(),
    content: @Composable () -> Unit
) {
    val colors = if (darkTheme) {
        DarkColorPalette
    } else {
        LightColorPalette
    }

    CompositionLocalProvider(LocalColors provides colors) {
        MaterialTheme(
            colors = colors.material,
            typography = Typography,
            shapes = Shapes,
            content = content
        )
    }
}

val MaterialTheme.myColors: MyColors
    @Composable
    @ReadOnlyComposable
    get() = LocalColors.current
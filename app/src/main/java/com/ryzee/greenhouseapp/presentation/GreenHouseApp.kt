package com.ryzee.greenhouseapp.presentation

import androidx.compose.animation.ExperimentalAnimationApi
import androidx.compose.runtime.Composable
import com.ryzee.greenhouseapp.manager.navigator.NavigatorManager
import com.ryzee.greenhouseapp.presentation.theme.GreenHouseTestAppTheme

@ExperimentalAnimationApi
@Composable
fun GreenHouseApp(navigatorManager: NavigatorManager) {
    GreenHouseTestAppTheme {
        AppNavGraph(navigatorManager)
    }
}
package com.ryzee.greenhouseapp.presentation.theme

import androidx.compose.material.Colors
import androidx.compose.ui.graphics.Color

data class MyColors(
    val material: Colors,
    val warningColor: Color,
) {
    val primary: Color get() = material.primary
    val primaryVariant: Color get() = material.primaryVariant
    val secondary: Color get() = material.secondary
    val secondaryVariant: Color get() = material.secondaryVariant
    val background: Color get() = material.background
    val surface: Color get() = material.surface
    val error: Color get() = material.error
    val onPrimary: Color get() = material.onPrimary
    val onSecondary: Color get() = material.onSecondary
    val onBackground: Color get() = material.onBackground
    val onSurface: Color get() = material.onSurface
    val onError: Color get() = material.onError
    val isLight: Boolean get() = material.isLight

    val purple200 = Color(0xFFBB86FC)
    val purple500 = Color(0xFF6200EE)
    val purple700 = Color(0xFF3700B3)
    val blue200 = Color(0xFF328BCF)
    val blue500 = Color(0xFF1e5591)
    val blue700 = Color(0xFF19487b)
    val teal200 = Color(0xFF03DAC5)
    val orange = Color(0xDDF5A632)
    val lightGray = Color(0xFFF1F1F1)

}
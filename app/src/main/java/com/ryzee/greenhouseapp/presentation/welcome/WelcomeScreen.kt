package com.ryzee.greenhouseapp.presentation.welcome

import android.Manifest
import androidx.activity.compose.ManagedActivityResultLauncher
import androidx.activity.compose.rememberLauncherForActivityResult
import androidx.activity.result.contract.ActivityResultContracts
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.*
import androidx.compose.material.*
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.unit.dp
import androidx.hilt.navigation.compose.hiltViewModel
import com.ryzee.greenhouseapp.R

@Composable
fun WelcomeScreen(viewModel: WelcomeViewModel = hiltViewModel()) {

    val launcher = rememberLauncherForActivityResult(
        contract = ActivityResultContracts.RequestPermission(),
        onResult = { granted ->
            viewModel.checkCameraPermission(granted)
        })

    Box(
        modifier = Modifier
            .fillMaxSize()
            .background(Color.White)
    ) {
        Column(
            modifier = Modifier.align(Alignment.Center),
            horizontalAlignment = Alignment.CenterHorizontally
        ) {
            Text(
                text = stringResource(id = R.string.welcome_greenhouse),
                style = MaterialTheme.typography.h6
            )
            Text(
                modifier = Modifier.padding(20.dp),
                text = stringResource(id = R.string.qr_scan_description),
            )
            Button(
                modifier = Modifier
                    .padding(50.dp)
                    .height(50.dp)
                    .width(250.dp),
                colors = ButtonDefaults.buttonColors(
                    backgroundColor = Color.Black,
                    contentColor = Color.White
                ),
                onClick = {
                    launchQrScanner(launcher)
                },
            ) {
                Text(text = stringResource(id = R.string.scan_qr_code))
            }
        }
    }
}

private fun launchQrScanner(launcher: ManagedActivityResultLauncher<String, Boolean>) {
    launcher.launch(Manifest.permission.CAMERA)
}

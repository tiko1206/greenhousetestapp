package com.ryzee.greenhouseapp.presentation.qrScan

import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.hilt.navigation.compose.hiltViewModel
import com.ryzee.greenhouseapp.presentation.qrScan.camera.CameraView

@Composable
fun QRScanScreen(viewModel: QRScanViewModel = hiltViewModel()) {
    LaunchedEffect(Unit) {
        viewModel.startScanning()
    }
    CameraView(
        isScanning = viewModel.isScanning,
        qrValidator = viewModel.qrValidator,
        onClick = {},
        onQrScanned = { result ->
            viewModel.onScanResult(result)
        },
    )
}
package com.ryzee.greenhouseapp.data.dataSource.api.provider

import com.ryzee.greenhouseapp.data.dataSource.api.service.PrivateApiService
import com.ryzee.greenhouseapp.data.dataSource.api.service.PublicApiService
import kotlinx.serialization.json.Json
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import javax.inject.Inject
import javax.inject.Singleton

sealed class ApiProvider<T : Any>(
    private val json: Json,
    private val okHttpBuilder: OkHttpClient.Builder,
    private val retrofitBuilder: Retrofit.Builder,
    private val apiInterfaceClass: Class<T>,
) {
    lateinit var api: T

    fun buildApi(url: String) {
        api = createApiInterface(url)
    }

    private fun createApiInterface(serverUrl: String) = retrofitBuilder
        .baseUrl(serverUrl)
        .client(okHttpBuilder.build())
        .build()
        .create(apiInterfaceClass)
}

@Singleton
class PublicApiProvider @Inject constructor(
    json: Json,
    okHttpBuilder: OkHttpClient.Builder,
    retrofitBuilder: Retrofit.Builder
) : ApiProvider<PublicApiService>(
    json,
    okHttpBuilder,
    retrofitBuilder,
    PublicApiService::class.java
)

@Singleton
class PrivateApiProvider @Inject constructor(
    json: Json,
    okHttpBuilder: OkHttpClient.Builder,
    retrofitBuilder: Retrofit.Builder
) : ApiProvider<PrivateApiService>(
    json,
    okHttpBuilder,
    retrofitBuilder,
    PrivateApiService::class.java
)
package com.ryzee.greenhouseapp.data.mapper.base

internal interface EntityMapper<M, E> {
    fun entityToCleanModel(entity: E): M
    fun entitiesToCleanModels(entityList: List<E>, vararg params: Any) =
        entityList.map { entityToCleanModel(it) }

    fun cleanModelToEntity(model: M): E
    fun cleanModelsToEntities(modelList: List<M>) = modelList.map { cleanModelToEntity(it) }
}
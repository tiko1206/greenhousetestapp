package com.ryzee.greenhouseapp.data.dataSource.api.service

import com.ryzee.greenhouseapp.data.model.response.UserResponse
import retrofit2.Response
import retrofit2.http.GET

interface PublicApiService {
    @GET("users")
    suspend fun getUsers(): Response<List<UserResponse>>
}
package com.ryzee.greenhouseapp.data.dataSource.api

import com.ryzee.greenhouseapp.data.model.ApiException
import com.ryzee.greenhouseapp.data.model.ApiResponse
import retrofit2.Response

suspend fun <R> enqueue(
    async: suspend () -> Response<R>,
    requestType: RequestType = RequestType.DEFAULT
): ApiResponse<R> {
    return try {
        val response = async()
        if(response.isSuccessful) {
            ApiResponse(result = response.body())
        } else {
            ApiResponse(exception = ApiException(response.code(), ""))
        }
    } catch (e: Exception) {
        ApiResponse(exception = e)
    }
}
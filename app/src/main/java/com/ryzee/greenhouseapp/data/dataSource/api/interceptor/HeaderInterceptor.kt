package com.ryzee.greenhouseapp.data.dataSource.api.interceptor

import okhttp3.Interceptor
import okhttp3.Response
import javax.inject.Inject

class HeaderInterceptor @Inject constructor(): Interceptor {

    companion object {
        const val APPLICATION_JSON = "application/json"
        const val CONTENT_TYPE = "Content-Type"
    }

    override fun intercept(chain: Interceptor.Chain): Response {
        val newBuilder = chain.request().newBuilder()

        newBuilder.apply {
            addHeader(CONTENT_TYPE, APPLICATION_JSON)
        }

        return chain.proceed(newBuilder.build())
    }
}
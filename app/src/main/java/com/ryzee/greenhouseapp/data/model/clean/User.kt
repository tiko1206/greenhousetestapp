package com.ryzee.greenhouseapp.data.model.clean

data class User(
    val id: String,
    val login: String,
    val name: String,
    val avatarUrl: String
)
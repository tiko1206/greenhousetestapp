package com.ryzee.greenhouseapp.data.repository.user

import com.ryzee.greenhouseapp.data.dataSource.api.provider.PublicApiProvider
import com.ryzee.greenhouseapp.data.mapper.UserMapper
import javax.inject.Inject

class UserRepoImpl @Inject constructor(
    private val apiProvider: PublicApiProvider,
    private val mapper: UserMapper
) : UserRepo {

}
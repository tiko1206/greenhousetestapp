package com.ryzee.greenhouseapp.data.mapper

import com.ryzee.greenhouseapp.data.mapper.base.ResponseMapper
import com.ryzee.greenhouseapp.data.model.clean.User
import com.ryzee.greenhouseapp.data.model.response.UserResponse
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class UserMapper @Inject constructor(): ResponseMapper<User, UserResponse> {
    override fun responseToCleanModel(response: UserResponse): User {
        return User(
            id = response.id ?: "",
            login = response.login ?: "",
            name = response.name ?: "",
            avatarUrl = response.avatarUrl ?: ""
        )
    }
}
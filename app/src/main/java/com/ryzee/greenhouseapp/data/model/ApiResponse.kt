package com.ryzee.greenhouseapp.data.model

data class ApiResponse<T>(
    val result: T? = null,
    val exception: Exception? = null
) {
    val isSuccess: Boolean
        get() = exception == null

    fun resultOrThrow(): T = result ?: throw exception ?: Exception()

    fun successResultOrThrow(): T {
        if(isSuccess && result != null) {
            return result
        }
        throw exception ?: Exception()
    }
}

fun <T, R> ApiResponse<T>.toResult(success: (T) -> Result<R>): Result<R> {
    if(exception != null) return Result.Error(exception)
    return success(resultOrThrow())
}

fun <T, R> ApiResponse<T>.wrapToResult(success: (T) -> R): Result<R> {
    if(exception != null) return Result.Error(exception)
    return Result.Success(success(resultOrThrow()))
}
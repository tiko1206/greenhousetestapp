package com.ryzee.greenhouseapp.data.model

data class ApiException(
    val code: Int,
    override val message: String,
) : Exception()
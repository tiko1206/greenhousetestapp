package com.ryzee.greenhouseapp.data.mapper.base

internal interface ResponseMapper<M, R> {
    fun responseToCleanModel(response: R): M
    fun responsesToCleanModels(responseList: List<R>) =
        responseList.map { responseToCleanModel(it) }
}
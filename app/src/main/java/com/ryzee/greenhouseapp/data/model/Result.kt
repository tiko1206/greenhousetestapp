package com.ryzee.greenhouseapp.data.model

sealed class Result<out R> {
    class Success<out T>(val data: T) : Result<T>()
    class Error(val exception: Exception? = null) : Result<Nothing>()
}